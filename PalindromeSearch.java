package stringWork;

import java.util.*;

public class PalindromeSearch {
    public static boolean isPalindrome(String word) {
        word = word.toLowerCase();
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) != word.charAt(word.length() - i - 1)) {
                return false;
            }
        }

        return true;
    }


    public static void main(String[] args) {
        String text = "����� ����� � ����� � ����� ����� � �����)?\"";
        String[] words = text.replaceAll("[^A-Za-z�-��-�-\s]", "").split(" ");
        ArrayList<String>  palindromes = new ArrayList<>();
        for (int i = 0; i < words.length; i++) {
            if (isPalindrome(words[i])) {
                palindromes.add(words[i]);
            }
        }

        for (int i = 0; i < palindromes.size(); i++) {
            System.out.println(palindromes.get(i));
        }
    }
}
