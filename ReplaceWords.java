package stringWork;

public class ReplaceWords
{
    public static void main(String[] args) {
        String text = "Object-oriented programming is a programming language model organized around objects rather than actions and data rather than logic. " +
                "Object-oriented programming blablblla " +
                "Object-oriented programming is a programming paradigm that relies on the concept of classes and objects. " +
                "Object-oriented programming blabla bla " +
                "Object-oriented programming is my favourite programming paradigm" ;

        String source = "object-oriented programming";
        String replacement = "OOP";
        String temporary = "";

        temporary = text.toLowerCase();

        if (temporary.contains(source)) {
            int replacementsInSource = (temporary.length() - temporary.replace(source, "").length()) / source.length();

            for (int i = replacementsInSource; i > 0; i--) {
                if (i % 2 == 0) {
                    text = text.substring(0, temporary.lastIndexOf(source)) + replacement + text.substring(temporary.lastIndexOf(source) + source.length());
                }
                temporary = temporary.substring(0, temporary.lastIndexOf(source)) + source.toUpperCase() + temporary.substring(temporary.lastIndexOf(source) + source.length());
            }

        }

        System.out.println(text);
    }

}