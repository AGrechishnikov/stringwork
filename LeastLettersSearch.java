package stringWork;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LeastLettersSearch {
    public static int lettersCount(String word) {
        int tempLength = word.length();
        Pattern pattern = Pattern.compile("(.)(?=.*(\\1))");
        Matcher matcher = pattern.matcher(word);
        while (matcher.find()) {
            tempLength--;
        }

        return tempLength;
    }

    public static void main(String[] args) {
        String text = "fffff ab f 1234 jkjk";
        String[] words = text.split(" ");
        int minIndex = 0;
        int minLength = lettersCount(words[0]);
        for (int i = 0; i < words.length; i++) {
            if (lettersCount(words[i]) < minLength) {
                minIndex = i;
                minLength = lettersCount(words[i]);
            }
            if (minLength == 1) {
                break;
            }
        }

        System.out.println(words[minIndex] + " " + minLength);
    }
}
