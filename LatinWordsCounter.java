package stringWork;

import java.util.regex.*;

public class LatinWordsCounter {
    public static boolean isAllLatinSymbols(String word) {
        for (int i = 0; i < word.length(); i++) {
            if (!(word.charAt(i) >= 65 && word.charAt(i) <= 90 || word.charAt(i) >= 97 && word.charAt(i) <= 122) ) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        String text = "��� ����������� � ����� english words � ���";
        String[] wordsInLine = text.replaceAll("[^A-Za-z�-��-�-\s]", "").split(" ");
        int count = 0;
        for (int i = 0; i < wordsInLine.length; i++) {
            if (isAllLatinSymbols(wordsInLine[i])) {
                count++;
            }
        }

        System.out.println(count);
    }
}
